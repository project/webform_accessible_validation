CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Usage


INTRODUCTION
------------

Current Maintainers:

 * Liz Pringi <https://www.drupal.org/u/epringi>

   Webform Accessible Validation increases the accessibility of form validation
with Webform forms by disabling HTML5 validation and changing the validation
error messages before the form into anchors linked to the form elements.


REQUIREMENTS
------------

Webform <https://www.drupal.org/project/webform>


INSTALLATION
------------

Clone the project's repository:

git clone --branch 7.x-1.x https://git.drupal.org/sandbox/epringi/2897947.git webform_accessible_validation


USAGE
-----

No settings available (for now), just download and enable and clear caches.
